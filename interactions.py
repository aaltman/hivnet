#!/usr/bin/python

import csv
import bulbs
from bulbs.model import Node, Relationship
from bulbs.neo4jserver import Graph
from time import time, strftime
import os

hiv_interaction_filename = r"hiv_interactions"
hprd_interaction_filename = os.path.join("HPRD_Release9_062910", "BINARY_PROTEIN_PROTEIN_INTERACTIONS.txt")
hiv_columns = (1, 6)
hprd_columns = (2, 5)

class Protein(Node):
    element_type = "protein"

class ProteinInteractsWith(Relationship):
    label = "interacts_with"

def init_graph():
    g = Graph()
    #g.add_proxy("proteins", Protein)
    #g.add_proxy("interactions", ProteinInteractsWith)
    return g

def insert_hiv_interactions(graph):
    r = csv.reader(open(hiv_interaction_filename), delimiter='\t')
    r.next() # Skip header line
    for line in r:
        v1 = graph.vertices.create(name = line[hiv_columns[0]])
        v2 = graph.vertices.create(name = line[hiv_columns[1]])
        graph.edges.create(v1, "interacts_with", v2)

def insert_hprd_interactions(graph):
    """Once HIV interactions have been inserted, include interactions from human proteins that are part of a connected component with the HIV proteins."""
    hprd_f = open(hprd_interaction_filename)
    r = csv.reader(hprd_f, delimiter='\t')

    # Pass over the data repeatedly until there are no more interactions to add.
    done = False
    while not done:
        done = True
        for line in r:
            if not graph.vertices.index.lookup(name=line[hprd_columns[0]]):
                v1 = graph.vertices.create(name = line[hprd_columns[0]])
                v2 = graph.vertices.create(name = line[hprd_columns[1]])
                graph.edges.create(v1, "interacts_with", v2)
                done = False

        # If at least one vertex was added this time through, rewind the file
        # and try again so we end up with the whole connected component.
        if not done: hprd_f.seek(0)

def main():
    fmt = "%I:%M:%S"
    print "\nImporting data into Neo4j.\n"

    start = time()
    g = init_graph()
    #elapsed = time() - start
#
#    print strftime(fmt) + ": done initializing graph.  Importing HIV interactions.\n"
#    insert_hiv_interactions(g)
#    elapsed = time() - start

    print strftime(fmt) + ": done importing HIV interactions.  Including whole human protein interaction connected component from HPRD.\n"
    insert_hprd_interactions(g)
    elapsed = time() - start
    print strftime(fmt) + ": Done.\n"

if __name__ == "__main__":
    main()
