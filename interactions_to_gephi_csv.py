#!/usr/bin/python

import csv

inputfn = r"interactions"
outputfn = "gephi_" + inputfn
w = open(outputfn, "w")
r = csv.reader(open(inputfn), dialect='excel', delimiter='\t')
col1 = 1
col2 = 6

for line in r:
    w.write(";".join([line[col1], line[col2]]))
    w.write("\n")
